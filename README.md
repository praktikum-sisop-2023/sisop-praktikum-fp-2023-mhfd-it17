# sisop-praktikum-fp-2023-MHFD-IT17

## Analisis Soal

Membuat sebuah sistem database sederhana dengan persyaratan sebagai berikut:

•	Program server harus berjalan sebagai daemon :
Ini berarti program server harus dapat berjalan di background sebagai layanan yang terus berjalan secara terpisah dari interaksi pengguna. Daemon sering digunakan untuk layanan jaringan atau tugas-tugas sistem yang membutuhkan keberadaan yang konstan.

•	Membuat program client sebagai akses console database:
Program client ini akan berfungsi sebagai antarmuka pengguna yang memungkinkan pengguna berinteraksi dengan sistem database. Pengguna akan dapat memasukkan perintah atau kueri melalui antarmuka ini, dan program client akan mengirimkan permintaan tersebut ke server database.

•	Program client harus dapat berinteraksi melalui socket dan dapat mengakses server dari mana saja :
Dalam konteks ini, interaksi melalui socket berarti program client akan menggunakan koneksi jaringan untuk berkomunikasi dengan server database. Ini memungkinkan program client dijalankan di mesin yang berbeda dengan server, sehingga pengguna dapat mengakses server database dari mana saja selama terhubung ke jaringan yang sama.

•	Program utama harus dapat berinteraksi melalui socket :
Ini berarti program utama, baik itu program server atau program client, akan menggunakan socket untuk berkomunikasi dengan pihak lain. Socket adalah mekanisme yang digunakan dalam pemrograman jaringan untuk mentransfer data antara komputer yang berbeda melalui jaringan.

Dengan demikian, tugas Anda adalah mengembangkan sistem database yang memenuhi persyaratan tersebut. Program server harus berjalan sebagai daemon yang melayani permintaan dari program client melalui koneksi socket. Program client harus dapat terhubung ke server dari mana saja melalui jaringan dan memungkinkan pengguna untuk berinteraksi dengan database melalui antarmuka console.

## Cara Pengerjaan client

## client.c
```sh
int main(int argc, char *argv[]) {
    int isRoot = 1;
    char *sudo_user = getenv("SUDO_USER");
    char *uName = NULL;

    if (sudo_user == NULL) {
        isRoot = 0;
        if (argc != 5) {
            printf("Invalid number of arguments.\n");
            return 1;
        }

        char *uPass = NULL;

        for (int i = 1; i < argc; i++) {
            if (strcmp(argv[i], "-u") == 0 && i + 1 < argc) {
                uName = argv[i + 1];
                i++;  // Skip the next argument since it's the value for -u
            } else if (strcmp(argv[i], "-p") == 0 && i + 1 < argc) {
                uPass = argv[i + 1];
                i++;  // Skip the next argument since it's the value for -p
            }
        }

        if (uName == NULL || uPass == NULL) {
            printf("Invalid arguments.\n");
            return 1;
        }
```
- Fungsi main memiliki dua parameter: argc (argumen count) yang menunjukkan jumlah argumen baris perintah yang diberikan saat menjalankan program, dan argv (argumen vector) yang berisi array string yang berisi argumen baris perintah itu sendiri.
- Variabel isRoot diinisialisasi dengan nilai 1, menandakan bahwa program dijalankan dengan hak akses root (superuser) secara default.
getenv("SUDO_USER") digunakan untuk mendapatkan nilai dari variabel lingkungan SUDO_USER yang berisi nama pengguna yang menjalankan program menggunakan sudo.
- Jika sudo_user bernilai NULL, berarti program tidak dijalankan dengan hak akses root.
- Jika argc tidak sama dengan 5 (jumlah argumen yang diharapkan), maka cetak pesan "Invalid number of arguments." dan program keluar dengan kode status 1.
- Jika kondisi di atas tidak terpenuhi, program mencari argumen baris perintah yang sesuai menggunakan loop for dengan variabel iterasi i.
Jika argumen saat ini adalah "-u" dan argumen berikutnya ada (i + 1 < argc), maka nilai uName diisi dengan argumen berikutnya dan argumen tersebut dilewati dengan menambahkan 1 pada variabel i.
- Jika argumen saat ini adalah "-p" dan argumen berikutnya ada (i + 1 < argc), maka nilai uPass diisi dengan argumen berikutnya dan argumen tersebut dilewati dengan menambahkan 1 pada variabel i.
Setelah selesai mencari argumen, dilakukan pemeriksaan apakah uName atau uPass bernilai NULL. Jika ya, cetak pesan "Invalid arguments." dan program keluar dengan kode status 1.


```sh
// Perform username and password verification
        FILE *file = fopen("../database/databases/users/users_list.txt", "r");
        if (file != NULL) {
            char line[256];
            fgets(line, sizeof(line), file); // Skip the header line
            int isAuthorized = 0;

            while (fgets(line, sizeof(line), file) != NULL) {
                // char *username = strtok(line, "- ");
                // char *password = strtok(NULL, " -");

                // Remove leading and trailing whitespace from the line
                char *trimmedLine = line;
                size_t lineLength = strlen(trimmedLine);

                while (lineLength > 0 && (trimmedLine[lineLength - 1] == '\n' || trimmedLine[lineLength - 1] == ' ')) {
                    trimmedLine[--lineLength] = '\0';
                }

                char *username = strtok(trimmedLine, " -");
                char *password = strtok(NULL, " -");

                if (strcmp(username, uName) == 0 && strcmp(password, uPass) == 0) {
                    isAuthorized = 1;
                    break;
                }
            }

            fclose(file);

            if (!isAuthorized) {
                printf("Invalid username or password.\n");
                return 1;
            }
        } 
        
        else {
            printf("User list file not found.\n");
            return 1;
        }

        isRoot = 0;

    } else {
        uName = "root";
    }
```
- Kode ini melakukan verifikasi username dan password dengan membuka file "users_list.txt" yang terletak di direktori "../database/databases/users/" menggunakan fungsi fopen.
- Jika file berhasil dibuka, kode membaca file baris per baris menggunakan fgets.
- Baris header pertama dilewati dengan menggunakan fgets sekali lagi.
- Variabel isAuthorized diinisialisasi dengan nilai 0 untuk menandakan bahwa belum ada otorisasi yang berhasil.
- Dalam loop while, setiap baris dari file dibaca dan diolah.
- Baris tersebut dipotong dari leading dan trailing whitespace menggunakan loop while dan strlen untuk menghilangkan spasi dan karakter newline di akhir baris.
- Baris yang telah dipotong kemudian dipecah menjadi dua bagian, yaitu username dan password, menggunakan fungsi strtok.
- Setelah username dan password terpisah, dilakukan pemeriksaan apakah nilai username sama dengan uName dan nilai password sama dengan uPass.
- Jika kedua kondisi terpenuhi, variabel isAuthorized diubah menjadi 1 dan loop while dihentikan dengan break.
Setelah selesai membaca file, file ditutup dengan menggunakan fclose.
- Jika isAuthorized masih bernilai 0 setelah loop while, cetak pesan "Invalid username or password." dan program keluar dengan kode status 1.
- Jika file tidak berhasil dibuka, cetak pesan "User list file not found." dan program keluar dengan kode status 1.
- Jika sudo_user bernilai NULL, variabel isRoot diubah menjadi 0 untuk menandakan bahwa program tidak dijalankan dengan hak akses root.
- Jika sudo_user tidak bernilai NULL, variabel uName diisi dengan nilai string "root".

```sh
   int clSocket;
    struct sockaddr_in srvAddr;
    char textCmd[BUF_SIZE];

    // Create a client socket
    clSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (clSocket == -1) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Set up server address
    srvAddr.sin_family = AF_INET;
    srvAddr.sin_port = htons(PORT);
    if (inet_pton(AF_INET, SERVER_IP, &(srvAddr.sin_addr)) <= 0) {
        perror("Invalid address/Address not supported");
        exit(EXIT_FAILURE);
    }

    // Connect to the server
    if (connect(clSocket, (struct sockaddr*)&srvAddr, sizeof(srvAddr)) == -1) {
        perror("Connection failed");
        exit(EXIT_FAILURE);
    }

    send(clSocket, uName, strlen(uName), 0);

    while (1) {
        // Read input from the user
        printf("Enter a message (or 'q' to quit): ");
        fgets(textCmd, sizeof(textCmd), stdin);

        // Send the message to the server
        send(clSocket, textCmd, strlen(textCmd), 0);

        // Check if user wants to quit
        if (textCmd[0] == 'q')
            break;

        // Receive and display the server's response
        memset(textCmd, 0, sizeof(textCmd));
        recv(clSocket, textCmd, sizeof(textCmd), 0);
        // printf("Server response: %s", textCmd);
    }

    // Close the socket
    close(clSocket);
    return 0;
}
```
- Variabel clSocket digunakan untuk menyimpan socket klien.
- Struktur sockaddr_in srvAddr digunakan untuk menyimpan informasi alamat server.
- Variabel textCmd digunakan untuk menyimpan pesan yang dikirim antara klien dan server.
- Membuat socket klien menggunakan fungsi socket dengan parameter AF_INET untuk domain alamat (IPv4), SOCK_STREAM untuk jenis soket (TCP), dan 0 untuk protokol default.
- Jika pembuatan soket gagal, cetak pesan error "Socket creation failed" dan keluar dari program.
- Mengatur alamat server menggunakan variabel srvAddr. Mengeset sin_family sebagai AF_INET (IPv4) dan sin_port sebagai PORT yang telah ditentukan.
- Mengonversi alamat IP server dari string ke representasi biner menggunakan inet_pton. Jika gagal, cetak pesan error "Invalid address/Address not supported" dan keluar dari program.
- Menghubungkan soket klien dengan server menggunakan connect. Jika gagal, cetak pesan error "Connection failed" dan keluar dari program.
- Mengirimkan nama pengguna (uName) ke server menggunakan send.
- Di dalam loop while yang berjalan selama kondisi 1 (true), mengambil input pesan dari pengguna menggunakan fgets.
- Mengirimkan pesan yang diinputkan oleh pengguna ke server menggunakan send.
- Memeriksa apakah pengguna ingin keluar dengan memeriksa karakter pertama dari textCmd. Jika karakter pertama adalah 'q', loop while dihentikan dengan break.
- Menerima respons dari server menggunakan recv dan menyimpannya di textCmd.
- Menutup soket klien menggunakan close.
- Program keluar dengan nilai status 0.

## Cara Pengerjaan server

## server.c

```sh
void unuseDb(char* str) {
    char* slashPtr = strchr(str, '/');
    if (slashPtr != NULL) {
        *slashPtr = '\0';
    }
}

int fileExists(const char* filename) {
    FILE* file = fopen(filename, "r");
    
    if (file != NULL) {
        fclose(file);
        return 1;
    }
    return 0;
}

int directoryExists(const char* dirPth) {
    if (access(dirPth, F_OK) == -1) {
        return 0; // Directory does not exist
    }
    
    return 1; // Directory exists
}
```

- Fungsi unuseDb menerima string (char*) sebagai parameter.
- Fungsi ini mencari karakter '/' pertama dalam string menggunakan strchr.
- Jika karakter '/' ditemukan, karakter tersebut diganti dengan '\0' (terminator string) sehingga string dipotong sebelum karakter '/'.
- Fungsi fileExists menerima nama file (const char*) sebagai parameter.
- Fungsi ini mencoba membuka file dengan mode "r" menggunakan fopen.
- Jika file berhasil dibuka, artinya file ada, maka file ditutup dan fungsi mengembalikan nilai 1.
- Jika file tidak dapat dibuka (NULL), artinya file tidak ada, maka fungsi mengembalikan nilai 0.
- Fungsi directoryExists menerima path direktori (const char*) sebagai parameter.
- Fungsi ini menggunakan access dengan mode F_OK untuk memeriksa apakah direktori ada.
- Jika access mengembalikan nilai -1, artinya direktori tidak ada, maka fungsi mengembalikan nilai 0.
- Jika access tidak mengembalikan -1, artinya direktori ada, maka fungsi mengembalikan nilai 1.

```sh
void create_table_file(const char* tableName, const char* columns) {
    char fileName[1024];
    strcpy(fileName, basePath);
    strcat(fileName, "/");
    strcat(fileName, tableName);
    strcat(fileName, ".txt");

    if (fileExists(fileName)) {
        printf("Table with the same name already exists..\n");
        return;
    }

    FILE* file = fopen(fileName, "w");
    if (file == NULL) {
        printf("Failed to create the table file.\n");
        return;
    }

    // Remove the unnecessary characters from the columns string
    char* columnsCopy = strdup(columns);
    char* token = strtok(columnsCopy, "( )\t\n\r");
    while (token != NULL) {
        if (strstr(token, "int") == NULL && strstr(token, "string") == NULL) {
            fprintf(file, "%s\t", token);
        }
            token = strtok(NULL, "( )\t\n\r");
        
    }
    fprintf(file, "\n");

    fclose(file);

    printf("Table file created successfully.\n");
    free(columnsCopy);
}
```

- Fungsi create_table_file menerima dua parameter, yaitu tableName (nama tabel) dan columns (kolom tabel dalam format string).
- Variabel fileName digunakan untuk menyimpan nama file tabel dengan ekstensi ".txt".
- Nama file dibentuk dengan menggabungkan basePath, "/", tableName, dan ".txt" menggunakan fungsi strcpy dan strcat.
- Dilakukan pengecekan apakah file dengan nama yang sama (fileName) sudah ada menggunakan fungsi fileExists.
- Jika file sudah ada, cetak pesan "Table with the same name already exists.." dan keluar dari fungsi.
- Membuka file untuk ditulis menggunakan fopen dengan mode "w".
- Jika file gagal dibuka (NULL), cetak pesan "Failed to create the table file." dan keluar dari fungsi.
- Dalam loop while, string columns disalin ke columnsCopy untuk keperluan pemrosesan.
- String columnsCopy dipotong menjadi token menggunakan strtok dengan delimiter "( )\t\n\r".
- Setiap token dicek apakah mengandung kata "int" atau "string" menggunakan strstr.
- Jika token tidak mengandung "int" dan "string", token tersebut ditulis ke dalam file menggunakan fprintf dengan delimiter "\t".
- Token selanjutnya diambil menggunakan strtok.
- Setelah selesai memproses semua token, ditulis newline ("\n") ke dalam file menggunakan fprintf.
- File ditutup menggunakan fclose.
- Cetak pesan "Table file created successfully.".
- Memory yang dialokasikan untuk columnsCopy dibebaskan menggunakan free.

```sh
Void log_command(const char *username, const char *command) {
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *log_data = fopen(LOG_FILE, "a");
    if (log_data) {
        fprintf(log_data, "%s:%s:%s\n", timestamp, username, command);
        fclose(log_data);
    }
}
```

- Fungsi log_command menerima dua parameter, yaitu username (nama pengguna) dan command (perintah yang akan dicatat).
- Variabel t digunakan untuk menyimpan waktu saat ini menggunakan time(NULL).
- Struct tm_info digunakan untuk menyimpan informasi waktu lokal menggunakan localtime(&t).
- Variabel timestamp digunakan untuk menyimpan waktu dalam format string dengan menggunakan strftime untuk memformat waktu sesuai dengan format "%Y-%m-%d %H:%M:%S".
- File log_data dibuka untuk ditulis dalam mode "a" (append) menggunakan fopen dengan LOG_FILE sebagai nama file log.
- Jika file berhasil dibuka, dilakukan penulisan ke dalam file menggunakan fprintf. Format penulisan adalah "%s:%s:%s\n" yang berarti menuliskan timestamp, username, dan command yang dipisahkan oleh tanda ":" dan diakhiri dengan newline.
- Setelah penulisan selesai, file log ditutup menggunakan fclose.
- Jika file tidak dapat dibuka, maka penulisan log tidak dilakukan.

```sh
void append_permission(const char *username, const char *permission) {
    FILE *file, *temp;
    char line[1024];

    file = fopen(FILENAME, "r");
    temp = fopen("temp.txt", "w");

    if (file == NULL) {
        printf("Error opening users_list.txt: %s\n", strerror(errno));
        return;
    }

    if (temp == NULL) {
        printf("Error opening temp.txt: %s\n", strerror(errno));
        return;
    }

    if (file == NULL || temp == NULL) {
        perror("Error opening files.");
        return;
    }

    while (fgets(line, sizeof(line), file)) {       
        // Remove newline character from the line
        line[strcspn(line, "\n")] = '\0';

        // Find the position of the username in the line
        char *pos = strstr(line, username);

        if (pos != NULL) {
            // Append the permission to the line
            fprintf(temp, "%s - %s\n", line, permission);
        } else {
            fputs(line, temp);
            fputc('\n', temp);  // Add a newline character if missing in the original line
        }
    }

```
- Fungsi append_permission menerima dua parameter, yaitu username (nama pengguna) dan permission (izin yang akan ditambahkan).
- Dua file pointer, file dan temp, dideklarasikan untuk membuka file utama dan file sementara.
- Variabel line digunakan untuk membaca setiap baris dalam file.
- File utama (FILENAME) dibuka dalam mode "r" menggunakan fopen.
- File sementara ("temp.txt") dibuka dalam mode "w" menggunakan fopen.
- Dilakukan pengecekan apakah file utama atau file sementara berhasil dibuka. Jika ada kesalahan, pesan kesalahan akan dicetak dan fungsi akan keluar.
- Selama masih ada baris yang dapat dibaca dari file utama, lakukan perulangan.
- Hapus karakter newline ("\n") dari baris menggunakan strcspn.
- Cari posisi username dalam baris menggunakan strstr.
- Jika username ditemukan dalam baris (pos bukan NULL), tulis baris tersebut ke file sementara dan tambahkan permission menggunakan fprintf.
- Jika username tidak ditemukan dalam baris, tulis baris tersebut ke file sementara tanpa perubahan menggunakan fputs.
- Tambahkan karakter newline ('\n') jika tidak ada dalam baris asli menggunakan fputc.
- Setelah selesai membaca semua baris, file utama dan file sementara ditutup menggunakan fclose.
- File utama ("users_list.txt") dan file sementara ("temp.txt") digunakan dalam operasi sistem operasi yang tidak ditampilkan dalam kode ini, seperti mengganti nama file atau menghapus file.

```sh
 fclose(file);
    fclose(temp);

    // Replace the original file with the temporary file
    if (rename("temp.txt", FILENAME) != 0) {
        perror("Error replacing file.");
        return;
    }

    printf("Permission appended successfully.\n");
}
```
- Fungsi fclose dipanggil untuk menutup file file dan temp setelah selesai membaca dan menulis.
- Dilakukan penggantian file asli dengan file sementara menggunakan fungsi rename. File sementara ("temp.txt") akan menggantikan file asli (FILENAME).
- Jika penggantian file tidak berhasil (mengembalikan nilai yang bukan nol), cetak pesan kesalahan menggunakan perror dan kembalikan fungsi.
- Jika penggantian file berhasil, cetak pesan "Permission appended successfully.".

```sh
int check_access(const char *username) {
    FILE *file = fopen(FILENAME, "r");
    if (file == NULL) {
        return -1;
    }

    char line[1024];
    int hasAccess = 0;  // Flag to track access

    while (fgets(line, sizeof(line), file)) {
        char *found = strstr(line, username);
        if (found != NULL) {
            char *access = strstr(found, "access");
            if (access != NULL) {
                hasAccess = 1;
                break;
            }
        }
    }

    fclose(file);

    return hasAccess;
}
```
- Fungsi check_access menerima satu parameter yaitu username (nama pengguna) yang akan dicek aksesnya.
- File pointer file dideklarasikan dan dibuka dalam mode "r" menggunakan fopen.
- Dilakukan pengecekan apakah file berhasil dibuka. Jika file tidak dapat dibuka, fungsi mengembalikan nilai -1 sebagai indikasi adanya kesalahan.
- Variabel line digunakan untuk membaca setiap baris dalam file.
- Variabel hasAccess dideklarasikan dan diinisialisasi dengan nilai 0 sebagai penanda bahwa akses belum ditemukan.
- Selama masih ada baris yang dapat dibaca dari file, lakukan perulangan.
- Cari posisi username dalam baris menggunakan strstr.
- Jika username ditemukan dalam baris (found tidak NULL), cari posisi "access" dalam baris menggunakan strstr.
- Jika "access" ditemukan dalam baris (access tidak NULL), set nilai hasAccess menjadi 1 dan keluar dari perulangan.
- Setelah selesai membaca semua baris, file ditutup menggunakan fclose.
- Fungsi mengembalikan nilai hasAccess yang menunjukkan apakah pengguna memiliki akses (nilai 1) atau tidak (nilai 0).

```sh
void handleClient(int clSocket) {
    char buffer[1024];
    int rdBytes;
    char use_database[100] = "";

    while (1) {
        rdBytes = recv(clSocket, buffer, sizeof(buffer), 0);
        if (rdBytes <= 0) {
            break;
        }
        buffer[rdBytes - 1] = '\0';
        printf("\nReceived: %s\n", buffer);

        char* createDbCmd = "CREATE DATABASE";
        char* createUserCmd = "CREATE USER";
        char* grantPermCmd = "GRANT PERMISSION";
        char* useDbCmd = "USE";
        char* createTableCmd = "CREATE TABLE";

        if (strstr(buffer, createDbCmd) != NULL) {
            char* dbNameStart = buffer + strlen(createDbCmd);
            while (*dbNameStart == ' ') {
                dbNameStart++;
            }
                
            char folderPath[100];
            sprintf(folderPath, "databases/%s", dbNameStart);
            // Create the database folder
            if (mkdir("databases", 0777) == 0) {
                printf("Created 'databases' folder: %s\n", folderPath);
            } 

            if (mkdir(folderPath, 0777) == 0) {
                printf("Created folder: %s\n", folderPath);
            } else {
                printf("Database creation failed folder: %s\n", folderPath);
            }
            log_command(uName, buffer);
        }
        
        else if (strstr(buffer, createUserCmd) != NULL) {
            char* usernameStart = buffer + strlen(createUserCmd);

            int check = check_access(uName);

            if(check || !strcmp(uName, "root")){
                while (*usernameStart == ' ') {
                usernameStart++;
            }
            char* passwordStart = strstr(usernameStart, "IDENTIFIED BY");
            if (passwordStart != NULL) {
                *passwordStart = '\0';
                passwordStart += strlen("IDENTIFIED BY");
                // Remove leading and trailing whitespace from username and password
                char* username = strtok(usernameStart, " ");
                char* password = strtok(passwordStart, " ");
                
                char folderPath[] = "databases/users";
                char filename[] = "databases/users/users_list.txt";  // Change file extension to txt

                if (mkdir("databases", 0777) == 0) {
                    printf("Created 'databases' folder\n");
                }

                // Create the 'users' folder if it doesn't exist
                if (mkdir(folderPath, 0777) == 0) {
                    printf("Created 'users' folder\n");
                }

                // Append the username and password to the text file
                FILE* file = fopen(filename, "a");
                if (file != NULL) {
                    // Check if the file is empty (to avoid writing the header repeatedly)
                    fseek(file, 0, SEEK_END);
                    long fileSize = ftell(file);
                    if (fileSize == 0) {
                        fprintf(file, "USERNAME - PASSWORD\n");  // Write the header if the file is empty
                    }
                    fprintf(file, "%s - %s\n", username, password);  // Write data
                    fclose(file);
                    printf("Created user: %s\n", username);
                } else {
                    printf("Failed to create user: %s\n", username);
                }

            }
            log_command(uName, buffer);
            }
            else {
        printf("Permission denied. Only 'root' can create users.\n");
    }
        }

        else if (strstr(buffer, grantPermCmd) != NULL){
            int check = check_access(uName);
            char dbName[100];
            char username[500];
            if(check || !strcmp(uName, "root")){
            sscanf(buffer, "GRANT PERMISSION %[^ ] INTO %s", dbName, username);
            append_permission(username, "access");
            log_command(uName, buffer);
            }
            else {
        printf("Permission denied. Only 'root' can create users.\n");
    }
        }

        else if (strstr(buffer, useDbCmd) != NULL) {
            char* dbTarget = buffer + strlen(useDbCmd);

            int check = check_access(uName);

            if(check || !strcmp(uName, "root")){
                while (*dbTarget == ' ') {
                    dbTarget++;
                }

                char dirName[1024];
                strcpy(dirName, basePath);
                strcat(dirName, "/");
                strcat(dirName, dbTarget);

                if (dbTarget == NULL || !directoryExists(dirName)) 
                {
                    printf("Unable to find database %s\n", dbTarget);
                    send(clSocket, buffer, strlen(buffer), 0);
                    memset(buffer, 0, sizeof(buffer));
                    continue;
                }

                unuseDb(basePath);

                strcat(basePath, "/");
                strcat(basePath, dbTarget);

                printf("Successfully used database %s\n", dbTarget);
                log_command(uName, buffer);
            }
            else {
                if (check == -1) {
                    printf("There are no users other than root.\n");
                    send(clSocket, buffer, strlen(buffer), 0);
                    memset(buffer, 0, sizeof(buffer));
                    continue;
                }
                printf("User %s doesn't have access to database %s\n", uName, dbTarget);
            }

        }

        else if (strstr(buffer, createTableCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char tableName[100];
            char columns[500];
            sscanf(buffer, "CREATE TABLE %[^ ] (%[^)]", tableName, columns);

            create_table_file(tableName, columns);
            log_command(uName, buffer);
        }

        else printf("Invalid command\n");

        send(clSocket, buffer, strlen(buffer), 0);
        memset(buffer, 0, sizeof(buffer));
    }

    close(clSocket);
}
```

- Fungsi handleClient menerima satu parameter yaitu clSocket yang merupakan socket untuk koneksi dengan klien.
- Deklarasi variabel-variabel yang diperlukan, seperti buffer untuk menyimpan data yang diterima, rdBytes untuk jumlah byte yang berhasil dibaca, dan use_database untuk menyimpan nama database yang sedang digunakan.
- Selama koneksi dengan klien aktif, lakukan perulangan.
- Terima data dari klien menggunakan recv.
- Periksa apakah ada kesalahan saat menerima data. Jika jumlah byte yang dibaca kurang dari atau sama dengan 0, keluar dari perulangan.
- Menghilangkan karakter newline (\n) dari data yang diterima.
- Cetak pesan yang diterima dari klien.
- Tentukan perintah yang diterima dengan membandingkan data yang diterima dengan string perintah yang valid, seperti CREATE DATABASE, CREATE USER, GRANT PERMISSION, USE, dan CREATE TABLE.
- Jika perintah yang diterima adalah CREATE DATABASE, ekstrak nama database dari data yang diterima dan buat folder untuk database tersebut.
- Jika perintah yang diterima adalah CREATE USER, ekstrak nama pengguna dan kata sandi dari data yang diterima, kemudian tambahkan pengguna ke file teks.
- Jika perintah yang diterima adalah GRANT PERMISSION, ekstrak nama database dan nama pengguna dari data yang diterima, lalu tambahkan izin akses untuk pengguna ke file teks.
- Jika perintah yang diterima adalah USE, ekstrak nama database dari data yang diterima, periksa apakah pengguna memiliki akses ke database tersebut, dan jika iya, ubah database yang digunakan.
- Jika perintah yang diterima adalah CREATE TABLE, ekstrak nama tabel dan kolom dari data yang diterima, kemudian buat file untuk tabel tersebut.
- Jika perintah yang diterima tidak valid, cetak pesan "Invalid command".
- Kirim data balasan ke klien menggunakan send.
- Kosongkan buffer untuk persiapan menerima data berikutnya.
- Setelah keluar dari perulangan, tutup socket klien.

```sh
void signalHandler(int signal) {
    if (signal == SIGCHLD) {
        pid_t chPID;
        int status;
        while ((chPID = waitpid(-1, &status, WNOHANG)) > 0) {
            // Child process terminated
            printf("User %s has disconnected from server.\n", uName);
        }
    }
}

```

- Fungsi signalHandler adalah penangan sinyal yang dipanggil ketika sinyal tertentu diterima.
- Fungsi ini menerima satu parameter yaitu signal, yang merupakan sinyal yang diterima.
- Periksa apakah sinyal yang diterima adalah SIGCHLD (sinyal yang dikirim saat proses anak berakhir).
- Jika sinyal adalah SIGCHLD, lakukan langkah-langkah berikut:
Deklarasikan variabel chPID bertipe pid_t untuk menyimpan ID proses anak.
Deklarasikan variabel status untuk menyimpan status keluaran dari proses anak.
Dalam perulangan, panggil waitpid dengan argumen -1 untuk menunggu setiap proses anak yang berakhir.
Jika nilai kembalian waitpid lebih dari 0, artinya ada proses anak yang berakhir.
Cetak pesan yang menyatakan bahwa pengguna (dengan nama uName) telah terputus dari server.


```sh
int main() {
    int srvSocket, clSocket;
    struct sockaddr_in srvAddress, clAddress;
    socklen_t addrSize;
    pid_t chPID;

    // Ignore child termination signals
    signal(SIGCHLD, signalHandler);

    // Create a server socket
    srvSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (srvSocket == -1) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Set up server address
    srvAddress.sin_family = AF_INET;
    srvAddress.sin_addr.s_addr = INADDR_ANY;
    srvAddress.sin_port = htons(PORT);

    // Bind the socket to the server address
    if (bind(srvSocket, (struct sockaddr*)&srvAddress, sizeof(srvAddress)) == -1) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen for client connections
    if (listen(srvSocket, MAX_CLIENTS) == -1) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server listening on port %d.\n", PORT);

    while (1) {
        // Accept client connections
        addrSize = sizeof(clAddress);
        clSocket = accept(srvSocket, (struct sockaddr*)&clAddress, &addrSize);
        if (clSocket == -1) {
            perror("Accept failed");
            continue;
        }

        recv(clSocket, uName, sizeof(uName), 0);
        printf("User %s connected to server.\n", uName);

        // Fork a child process to handle the client
        chPID = fork();
        if (chPID == 0) {
            // Child process
            close(srvSocket);
            handleClient(clSocket);
            exit(EXIT_SUCCESS);
        } else if (chPID > 0) {
            // Parent process
            close(clSocket);
        } else {
            // Fork error
            perror("Fork failed");
            exit(EXIT_FAILURE);
        }
    }

    close(srvSocket);
    return 0;
}
```
- Fungsi main adalah titik masuk utama untuk program server.
- Deklarasikan variabel srvSocket dan clSocket untuk socket server dan socket klien.
- Deklarasikan struktur srvAddress dan clAddress untuk alamat server dan alamat klien.
- Deklarasikan variabel addrSize untuk menyimpan ukuran alamat.
- Deklarasikan variabel chPID untuk menyimpan ID proses anak.
- Mengabaikan sinyal terminasi proses anak dengan memanggil signal(SIGCHLD, signalHandler).
- Buat soket server dengan menggunakan socket(AF_INET, SOCK_STREAM, 0).
- Setel alamat server dengan mengisi struktur srvAddress.
- Ikat soket dengan alamat server menggunakan bind.
- Dengarkan koneksi dari klien menggunakan listen.
- Dalam loop utama:
Terima koneksi dari klien menggunakan accept.
Terima nama pengguna (uName) dari klien menggunakan recv.
Fork proses anak untuk menangani klien menggunakan fork.
Jika chPID adalah 0, ini adalah proses anak:
Tutup soket server menggunakan close.
Panggil handleClient untuk menangani klien menggunakan clSocket.
Keluar dari proses anak menggunakan exit.
Jika chPID lebih dari 0, ini adalah proses induk:
Tutup soket klien menggunakan close.
Jika chPID kurang dari 0, terjadi kesalahan dalam forking.
- Tutup soket server menggunakan close.



## Source Code

## client.c

```sh
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define SERVER_IP "127.0.0.1"
#define PORT 8888
#define BUF_SIZE 1024

int main(int argc, char *argv[]) {
    int isRoot = 1;
    char *sudo_user = getenv("SUDO_USER");
    char *uName = NULL;

    if (sudo_user == NULL) {
        isRoot = 0;
        if (argc != 5) {
            printf("Invalid number of arguments.\n");
            return 1;
        }

        char *uPass = NULL;

        for (int i = 1; i < argc; i++) {
            if (strcmp(argv[i], "-u") == 0 && i + 1 < argc) {
                uName = argv[i + 1];
                i++;  // Skip the next argument since it's the value for -u
            } else if (strcmp(argv[i], "-p") == 0 && i + 1 < argc) {
                uPass = argv[i + 1];
                i++;  // Skip the next argument since it's the value for -p
            }
        }

        if (uName == NULL || uPass == NULL) {
            printf("Invalid arguments.\n");
            return 1;
        }

        // Perform username and password verification
        FILE *file = fopen("../database/databases/users/users_list.txt", "r");
        if (file != NULL) {
            char line[256];
            fgets(line, sizeof(line), file); // Skip the header line
            int isAuthorized = 0;

            while (fgets(line, sizeof(line), file) != NULL) {
                // char *username = strtok(line, "- ");
                // char *password = strtok(NULL, " -");

                // Remove leading and trailing whitespace from the line
                char *trimmedLine = line;
                size_t lineLength = strlen(trimmedLine);

                while (lineLength > 0 && (trimmedLine[lineLength - 1] == '\n' || trimmedLine[lineLength - 1] == ' ')) {
                    trimmedLine[--lineLength] = '\0';
                }

                char *username = strtok(trimmedLine, " -");
                char *password = strtok(NULL, " -");

                if (strcmp(username, uName) == 0 && strcmp(password, uPass) == 0) {
                    isAuthorized = 1;
                    break;
                }
            }

            fclose(file);

            if (!isAuthorized) {
                printf("Invalid username or password.\n");
                return 1;
            }
        } 
        
        else {
            printf("User list file not found.\n");
            return 1;
        }

        isRoot = 0;

    } else {
        uName = "root";
    }

    int clSocket;
    struct sockaddr_in srvAddr;
    char textCmd[BUF_SIZE];

    // Create a client socket
    clSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (clSocket == -1) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Set up server address
    srvAddr.sin_family = AF_INET;
    srvAddr.sin_port = htons(PORT);
    if (inet_pton(AF_INET, SERVER_IP, &(srvAddr.sin_addr)) <= 0) {
        perror("Invalid address/Address not supported");
        exit(EXIT_FAILURE);
    }

    // Connect to the server
    if (connect(clSocket, (struct sockaddr*)&srvAddr, sizeof(srvAddr)) == -1) {
        perror("Connection failed");
        exit(EXIT_FAILURE);
    }

    send(clSocket, uName, strlen(uName), 0);

    while (1) {
        // Read input from the user
        printf("Enter a message (or 'q' to quit): ");
        fgets(textCmd, sizeof(textCmd), stdin);

        // Send the message to the server
        send(clSocket, textCmd, strlen(textCmd), 0);

        // Check if user wants to quit
        if (textCmd[0] == 'q')
            break;

        // Receive and display the server's response
        memset(textCmd, 0, sizeof(textCmd));
        recv(clSocket, textCmd, sizeof(textCmd), 0);
        // printf("Server response: %s", textCmd);
    }

    // Close the socket
    close(clSocket);
    return 0;
}
```

## server.c

```sh
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <errno.h>
#include <ctype.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <signal.h>
#include <dirent.h>

#define PORT 8888
#define MAX_CLIENTS 5
#define FILENAME "databases/users/users_list.txt"
#define LOG_FILE "log.txt"

char basePath[1024] = "databases";
char uName[256];

void unuseDb(char* str) {
    char* slashPtr = strchr(str, '/');
    if (slashPtr != NULL) {
        *slashPtr = '\0';
    }
}

int fileExists(const char* filename) {
    FILE* file = fopen(filename, "r");
    
    if (file != NULL) {
        fclose(file);
        return 1;
    }
    return 0;
}

int directoryExists(const char* dirPth) {
    if (access(dirPth, F_OK) == -1) {
        return 0; // Directory does not exist
    }
    
    return 1; // Directory exists
}

void create_table_file(const char* tableName, const char* columns) {
    char fileName[1024];
    strcpy(fileName, basePath);
    strcat(fileName, "/");
    strcat(fileName, tableName);
    strcat(fileName, ".txt");

    if (fileExists(fileName)) {
        printf("Table with the same name already exists..\n");
        return;
    }

    FILE* file = fopen(fileName, "w");
    if (file == NULL) {
        printf("Failed to create the table file.\n");
        return;
    }

    // Remove the unnecessary characters from the columns string
    char* columnsCopy = strdup(columns);
    char* token = strtok(columnsCopy, "( )\t\n\r");
    while (token != NULL) {
        if (strstr(token, "int") == NULL && strstr(token, "string") == NULL) {
            fprintf(file, "%s\t", token);
        }
            token = strtok(NULL, "( )\t\n\r");
        
    }
    fprintf(file, "\n");

    fclose(file);

    printf("Table file created successfully.\n");
    free(columnsCopy);
}

void log_command(const char *username, const char *command) {
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *log_data = fopen(LOG_FILE, "a");
    if (log_data) {
        fprintf(log_data, "%s:%s:%s\n", timestamp, username, command);
        fclose(log_data);
    }
}

void append_permission(const char *username, const char *permission) {
    FILE *file, *temp;
    char line[1024];

    file = fopen(FILENAME, "r");
    temp = fopen("temp.txt", "w");

    if (file == NULL) {
        printf("Error opening users_list.txt: %s\n", strerror(errno));
        return;
    }

    if (temp == NULL) {
        printf("Error opening temp.txt: %s\n", strerror(errno));
        return;
    }

    if (file == NULL || temp == NULL) {
        perror("Error opening files.");
        return;
    }

    while (fgets(line, sizeof(line), file)) {       
        // Remove newline character from the line
        line[strcspn(line, "\n")] = '\0';

        // Find the position of the username in the line
        char *pos = strstr(line, username);

        if (pos != NULL) {
            // Append the permission to the line
            fprintf(temp, "%s - %s\n", line, permission);
        } else {
            fputs(line, temp);
            fputc('\n', temp);  // Add a newline character if missing in the original line
        }
    }

    fclose(file);
    fclose(temp);

    // Replace the original file with the temporary file
    if (rename("temp.txt", FILENAME) != 0) {
        perror("Error replacing file.");
        return;
    }

    printf("Permission appended successfully.\n");
}

int check_access(const char *username) {
    FILE *file = fopen(FILENAME, "r");
    if (file == NULL) {
        return -1;
    }

    char line[1024];
    int hasAccess = 0;  // Flag to track access

    while (fgets(line, sizeof(line), file)) {
        char *found = strstr(line, username);
        if (found != NULL) {
            char *access = strstr(found, "access");
            if (access != NULL) {
                hasAccess = 1;
                break;
            }
        }
    }

    fclose(file);

    return hasAccess;
}

void handleClient(int clSocket) {
    char buffer[1024];
    int rdBytes;
    char use_database[100] = "";

    while (1) {
        rdBytes = recv(clSocket, buffer, sizeof(buffer), 0);
        if (rdBytes <= 0) {
            break;
        }
        buffer[rdBytes - 1] = '\0';
        printf("\nReceived: %s\n", buffer);

        char* createDbCmd = "CREATE DATABASE";
        char* createUserCmd = "CREATE USER";
        char* grantPermCmd = "GRANT PERMISSION";
        char* dropDbCmd = "DROP DATABASE";
        char* useDbCmd = "USE";
        char* createTableCmd = "CREATE TABLE";

        if (strstr(buffer, createDbCmd) != NULL) {
            char* dbNameStart = buffer + strlen(createDbCmd);
            while (*dbNameStart == ' ') {
                dbNameStart++;
            }
                
            char folderPath[100];
            sprintf(folderPath, "databases/%s", dbNameStart);
            // Create the database folder
            if (mkdir("databases", 0777) == 0) {
                printf("Created 'databases' folder: %s\n", folderPath);
            } 

            if (mkdir(folderPath, 0777) == 0) {
                printf("Created folder: %s\n", folderPath);
            } else {
                printf("Database creation failed folder: %s\n", folderPath);
            }
            log_command(uName, buffer);
        }
        
        else if (strstr(buffer, createUserCmd) != NULL) {
            char* usernameStart = buffer + strlen(createUserCmd);
            while (*usernameStart == ' ') {
                usernameStart++;
            }
            char* passwordStart = strstr(usernameStart, "IDENTIFIED BY");
            if (passwordStart != NULL) {
                *passwordStart = '\0';
                passwordStart += strlen("IDENTIFIED BY");
                // Remove leading and trailing whitespace from username and password
                char* username = strtok(usernameStart, " ");
                char* password = strtok(passwordStart, " ");
                
                char folderPath[] = "databases/users";
                char filename[] = "databases/users/users_list.txt";  // Change file extension to txt

                if (mkdir("databases", 0777) == 0) {
                    printf("Created 'databases' folder\n");
                }

                // Create the 'users' folder if it doesn't exist
                if (mkdir(folderPath, 0777) == 0) {
                    printf("Created 'users' folder\n");
                }

                // Append the username and password to the text file
                FILE* file = fopen(filename, "a");
                if (file != NULL) {
                    // Check if the file is empty (to avoid writing the header repeatedly)
                    fseek(file, 0, SEEK_END);
                    long fileSize = ftell(file);
                    if (fileSize == 0) {
                        fprintf(file, "USERNAME - PASSWORD\n");  // Write the header if the file is empty
                    }
                    fprintf(file, "%s - %s\n", username, password);  // Write data
                    fclose(file);
                    printf("Created user: %s\n", username);
                } else {
                    printf("Failed to create user: %s\n", username);
                }

            }
            log_command(uName, buffer);
        }

        else if (strstr(buffer, grantPermCmd) != NULL){
            char dbName[100];
            char username[500];
            sscanf(buffer, "GRANT PERMISSION %[^ ] INTO %s", dbName, username);
            append_permission(username, "access");
            log_command(uName, buffer);
        }

        else if (strstr(buffer, dropDbCmd) != NULL) {
            int check, access = 0;

            char* dbTarget = buffer + strlen(dropDbCmd);
            while (*dbTarget == ' ') {
                dbTarget++;
            }

            if (dbTarget == NULL) {
                printf("Invalid input\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            // if (sudo) access = 1;
            // else {
            //     checker = find_database(dbTarget, username);
            //     access = checker;
            // }
            access = 1;

            log_command(uName, buffer);
        }

        else if (strstr(buffer, useDbCmd) != NULL) {
            char* dbTarget = buffer + strlen(useDbCmd);

            int check = check_access(uName);

            if(check || !strcmp(uName, "root")){
                while (*dbTarget == ' ') {
                    dbTarget++;
                }

                char dirName[1024];
                strcpy(dirName, basePath);
                strcat(dirName, "/");
                strcat(dirName, dbTarget);

                if (dbTarget == NULL || !directoryExists(dirName)) 
                {
                    printf("Unable to find database %s\n", dbTarget);
                    send(clSocket, buffer, strlen(buffer), 0);
                    memset(buffer, 0, sizeof(buffer));
                    continue;
                }

                unuseDb(basePath);

                strcat(basePath, "/");
                strcat(basePath, dbTarget);

                printf("Successfully used database %s\n", dbTarget);
                log_command(uName, buffer);
            }
            else {
                if (check == -1) {
                    printf("There are no users other than root.\n");
                    send(clSocket, buffer, strlen(buffer), 0);
                    memset(buffer, 0, sizeof(buffer));
                    continue;
                }
                printf("User %s doesn't have access to database %s\n", uName, dbTarget);
            }

        }

        else if (strstr(buffer, createTableCmd) != NULL) {
            if (strchr(basePath, '/') == NULL) {
                printf("Please USE a database first!\n");
                send(clSocket, buffer, strlen(buffer), 0);
                memset(buffer, 0, sizeof(buffer));
                continue;
            }

            char tableName[100];
            char columns[500];
            sscanf(buffer, "CREATE TABLE %[^ ] (%[^)]", tableName, columns);

            create_table_file(tableName, columns);
            log_command(uName, buffer);
        }

        else printf("Invalid command\n");

        send(clSocket, buffer, strlen(buffer), 0);
        memset(buffer, 0, sizeof(buffer));
    }

    close(clSocket);
}

void signalHandler(int signal) {
    if (signal == SIGCHLD) {
        pid_t chPID;
        int status;
        while ((chPID = waitpid(-1, &status, WNOHANG)) > 0) {
            // Child process terminated
            printf("User %s has disconnected from server.\n", uName);
        }
    }
}

int main() {
    int srvSocket, clSocket;
    struct sockaddr_in srvAddress, clAddress;
    socklen_t addrSize;
    pid_t chPID;

    // Ignore child termination signals
    signal(SIGCHLD, signalHandler);

    // Create a server socket
    srvSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (srvSocket == -1) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Set up server address
    srvAddress.sin_family = AF_INET;
    srvAddress.sin_addr.s_addr = INADDR_ANY;
    srvAddress.sin_port = htons(PORT);

    // Bind the socket to the server address
    if (bind(srvSocket, (struct sockaddr*)&srvAddress, sizeof(srvAddress)) == -1) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen for client connections
    if (listen(srvSocket, MAX_CLIENTS) == -1) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server listening on port %d.\n", PORT);

    while (1) {
        // Accept client connections
        addrSize = sizeof(clAddress);
        clSocket = accept(srvSocket, (struct sockaddr*)&clAddress, &addrSize);
        if (clSocket == -1) {
            perror("Accept failed");
            continue;
        }

        recv(clSocket, uName, sizeof(uName), 0);
        printf("User %s connected to server.\n", uName);

        // Fork a child process to handle the client
        chPID = fork();
        if (chPID == 0) {
            // Child process
            close(srvSocket);
            handleClient(clSocket);
            exit(EXIT_SUCCESS);
        } else if (chPID > 0) {
            // Parent process
            close(clSocket);
        } else {
            // Fork error
            perror("Fork failed");
            exit(EXIT_FAILURE);
        }
    }

    close(srvSocket);
    return 0;
}
```

## Tes Output
![image](https://i.ibb.co/hKybPVC/Whats-App-Image-2023-06-25-at-16-05-33.jpg)
![image](https://i.ibb.co/3rHwK2z/Whats-App-Image-2023-06-25-at-16-07-00.jpg)
