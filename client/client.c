#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <errno.h>
#include <ctype.h>

#define SERVER "127.0.0.1"
#define BUF_SIZE 1024  
#define PORT 8888
#define USERS_FILE "../database/users.txt"
#define LOG_FILE "../database/log.txt"
#define DATABASE_DIR "../database/"

void die(char *s) {
    perror(s);
    exit(1);
}

int authenticate(char *username, char *password) {
    char line[256];
    char *user, *pass;

    FILE *file = fopen(USERS_FILE, "r");
    if (!file) {
        die("Could not open users file");
    }

    while (fgets(line, sizeof(line), file)) {
        user = strtok(line, ":");
        pass = strtok(NULL, ":");
        if (pass[strlen(pass) - 1] == '\n') pass[strlen(pass) - 1] = 0; // Remove newline character

        if (strcmp(username, user) == 0 && strcmp(password, pass) == 0) {
            fclose(file);
            return 1; // User is authenticated
        }
    }

    fclose(file);
    return 0; // User is not authenticated
}

void create_user(char *username, char *password) {
    FILE *file = fopen(USERS_FILE, "a");
    if (!file) {
        die("Could not open users file");
    }

    fprintf(file, "%s:%s\n", username, password);
    fclose(file);
}

void log_command(char *username, char *command) {
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];

    strftime(timestamp, 20, "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *file = fopen(LOG_FILE, "a");
    if (!file) {
        die("Could not open log file");
    }

    fprintf(file, "%s:%s:%s\n", timestamp, username, command);
    fclose(file);
}

int is_valid_command(char *command) {
    return strncmp(command, "CREATE USER ", 12) == 0 || strncmp(command, "CREATE DATABASE ", 16) == 0 || strncmp(command, "USE ", 4) == 0;
}

int is_valid_db_name(char *db_name) {
    for (int i = 0; i < strlen(db_name); i++) {
        if (!isalnum(db_name[i])) {
            return 0;
        }
    }

    return 1;
}

int create_database(char *db_name) {
    if (!is_valid_db_name(db_name)) {
        printf("Invalid database name. Only alphanumeric characters are allowed.\n");
        return -1;
    }
    char db_dir[256];
    snprintf(db_dir, sizeof(db_dir), "%s%s", DATABASE_DIR, db_name);

    if (mkdir(db_dir, 0755) == -1) {
        printf("Database creation failed: %s.\n", strerror(errno));
        return -1;
    }

    printf("Database created successfully.\n");
    return 0;
}

int main(int argc, char **argv) {
    char *username, *password;
    char current_db[256] = "";

    if (geteuid() != 0 && argc != 5) {
        printf("Usage: %s -u <username> -p <password>\n", argv[0]);
        exit(1);
    } else if (geteuid() == 0) {
        username = "root";
        password = "root";
    } else {
        if (strcmp(argv[1], "-u") == 0) {
            username = argv[2];
        } else {
            die("Invalid arguments");
        }

        if (strcmp(argv[3], "-p") == 0) {
            password = argv[4];
        } else {
            die("Invalid arguments");
        }
    }

    if (!authenticate(username, password)) {
        printf("Authentication failed.\n");
        exit(1);
    }

    struct sockaddr_in si_other;
    int s, i, slen=sizeof(si_other);
    char buf[BUF_SIZE];
    char message[BUF_SIZE];

    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        die("socket");
    }

    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);

    if (inet_aton(SERVER , &si_other.sin_addr) == 0) {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }

    while(1) {
        printf("Enter message : ");
        fgets(message, BUF_SIZE, stdin);

        if (!is_valid_command(message)) {
            printf("Invalid command. Usage: CREATE USER [user] IDENTIFIED BY [password]; or CREATE DATABASE [nama_database]; or USE [nama_database];\n");
            continue;
        }

        if (geteuid() == 0 || strcmp(username, "root") == 0) { // User is root
            if (strncmp(message, "CREATE USER ", 12) == 0) {
                char *new_username = strtok(message + 12, " ");
                if (!new_username) {
                    printf("Invalid command syntax.\n");
                    continue;
                }
                if (strncmp(new_username + strlen(new_username) + 1, "IDENTIFIED BY ", 14) != 0) {
                    printf("Invalid command syntax.\n");
                    continue;
                }
                char *new_password = new_username + strlen(new_username) + 15;
                if (!new_password) {
                    printf("Invalid command syntax.\n");
                    continue;
                }
                new_password[strlen(new_password) - 1] = 0; // Remove newline character

                create_user(new_username, new_password);
                printf("User created successfully.\n");
                log_command(username, message);
                continue;
            }
        }

        if (strncmp(message, "CREATE DATABASE ", 16) == 0) {
            char *db_name = strtok(message + 16, ";");
            if (!db_name) {
                printf("Invalid command syntax.\n");
                continue;
            }

            // Remove newline if present
            db_name[strcspn(db_name, "\n")] = 0;

            create_database(db_name);
            log_command(username, message);
            continue;
        }

        if (strncmp(message, "USE ", 4) == 0) {
            char *db_name = strtok(message + 4, " ");
            if (!db_name) {
                printf("Invalid command syntax.\n");
                continue;
            }
            db_name[strlen(db_name) - 1] = 0; // Remove newline character

            // Check if the database exists
            char db_dir[256];
            snprintf(db_dir, sizeof(db_dir), "%s%s", DATABASE_DIR, db_name);
            if (access(db_dir, F_OK) != -1) {
                // Database exists
                strcpy(current_db, db_name);
                printf("Switched to database %s.\n", db_name);
            } else {
                // Database doesn't exist
                printf("Database %s doesn't exist.\n", db_name);
                continue;
            }

            log_command(username, message);
            continue;
        }

        if (sendto(s, message, strlen(message), 0, (struct sockaddr *) &si_other, slen)==-1) {
            die("sendto()");
        }

        log_command(username, message);

        memset(buf,'\0', BUF_SIZE);
        if (recvfrom(s, buf, BUF_SIZE, 0, (struct sockaddr *) &si_other, &slen) == -1) {
            die("recvfrom()");
        }

        puts(buf);
    }

    close(s);
    return 0;
}